import pygame
import pickle

colour_files = 'OceanColorsSorted.bat', 'OceanColorsSortedJS.bat', 'OceanColorsSortedRGB.bat'
sorted_file = 'FinalColorsSorted.bat' # If saved

TheColorFile = colour_files[1] # Only thing that can be changed. Change to any of the above file paths
        
# Dont Worry About Anything Down Here.

colours_length = 101
def swap_color(color_array, i, j):

    color_array[i], color_array[j] = color_array[j].copy(), color_array[i].copy()
    pygame.draw.rect(surface, color_array[i], (i*10, 0, 10, 600))
    pygame.draw.rect(surface, color_array[j], (j*10, 0, 10, 600))
    return color_array

pygame.init()
screen = pygame.display.set_mode((colours_length*10, 620))
surface = pygame.Surface((colours_length, 1))

color_array = pickle.load(open(TheColorFile, 'rb'))
for i, color in enumerate(color_array):
    surface.set_at((i, 0), color)

surface = pygame.transform.scale(surface, (colours_length*10, 600))

selected_index_a = None
selected_index_b = None
selected_index_1 = None
selected_index_2 = None

while True:

    pygame.display.update()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            raise SystemExit
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                print('DeSelected', selected_index_1)
                selected_index_1 = None
            if event.key == pygame.K_RETURN:
                pickle.dump(color_array, open('FinalColorsSorted.bat', 'wb'))
                print('\n FileSaved as "FinalColorsSorted.bat" \n')

        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            
            for i in range(colours_length):
                rect = pygame.Rect(i*10, 0, 10, 600)
                if rect.collidepoint(event.pos):
                    if selected_index_1 is None:
                        selected_index_1 = i
                        selected_index_a = i
                        selected_index_b = None
                        print('selected', i)
                        break
                    else:
                        selected_index_2 = i
                        color_array = swap_color(color_array, selected_index_1, selected_index_2)
                        print(f'Swapped {selected_index_1} and {selected_index_2}', '\n==============================')
                        selected_index_b = i
                        selected_index_1 = None
                        selected_index_2 = None

    screen.fill((0, 0, 0))
    if selected_index_1 is not None:
        pygame.draw.rect(screen, (255, 0, 255), (selected_index_1*10, 600, 10, 20))
    if selected_index_b is not None:
        pygame.draw.rect(screen, (255, 255, 0), (selected_index_a*10, 600, 10, 20))
        pygame.draw.rect(screen, (255, 255, 0), (selected_index_b*10, 600, 10, 20))
    screen.blit(surface, (0, 0))