Run ColorGradient.pyw in the terminal so that the printed messages can be seen.

In the script there are a few color file paths, Choose any.
Available color files:
    colour_files[0]
    colour_files[1]
    colour_files[2]
    sorted_file     # if saved
    
`colour_files[1]` Default.
Each file has a different order of colors.

To swap two colors, left-click on the first color. It will me marked with Magenta at the bottom of the screen
Left-Click on the second color to swap the colors. Both the swapped colors will be marked with Yellow at the bottom of the screen

To cancel the first selection, Press Esc. The Magenta marker will be removed.

Keep the Deep Ocean Colors to the Left and Sandy Colors to the Right.
Once you are satisfied with the order of the colors, press Enter to save the colors to "FinalColorGradient.bat"

Please share the Final file.

Thank You For Everything!

